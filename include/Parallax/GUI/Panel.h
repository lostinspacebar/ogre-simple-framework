/*
 * Simple 2D Panel
 * 
 * ------------------------------------------------------------------
 * Parallax Ogre3D Helper Framework
 * Copyright 2013
 * Aditya Gaddam
 * http://lostinspacebar.com/project/parallax
 */
 
#ifndef PARALLAX_GUI_PANEL_H_
#define PARALLAX_GUI_PANEL_H_

#include <Parallax/GUI/Element.h>
#include <OgreOverlayElement.h>
#include <OgreMaterialManager.h>
#include <OgreTechnique.h>

namespace lostinspacebar
{
namespace parallax
{
namespace gui
{
	class Panel : public Element
	{
	public:
		
		/**
		 * Constructor
		 *
		 */
		Panel(Ogre::String name, Ogre::Vector2 position = Ogre::Vector2(0, 0), 
								 Ogre::Vector2 size = Ogre::Vector2(100, 100), 
								 Ogre::ColourValue colour = Ogre::ColourValue(1.0f, 1.0f, 1.0f));
		
		/**
		 * Sets the position of the element on screen
		 *
		 */
		virtual void setPosition(Ogre::Vector2 position)
		{
			mElement->setLeft(position.x);
			mElement->setTop(position.y);
			
			Element::setPosition(position);
		}
		
		/**
		 * Sets the size of the element on screen
		 *
		 */
		virtual void setSize(Ogre::Vector2 size)
		{
			mElement->setWidth(size.x);
			mElement->setHeight(size.y);			
			
			Element::setSize(size);
		}
		
		/**
		 * Sets the background color for the element
		 *
		 * @param colour	Colour for the background of the element
		 */
		virtual void setBackgroundColour(Ogre::ColourValue colour)
		{
			mMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);
			Element::setBackgroundColour(colour);
		}
		
		/**
		 * Sets the texture for the panel
		 *
		 * @param textureName		Name of the texture in the Ogre system
		 */
		virtual void setTexture(Ogre::String textureName)
		{
			mMaterial->getTechnique(0)->getPass(0)->createTextureUnitState(textureName);
		}
		
		/**
		 * Gets the underlying OGRE overlay element that 
		 * can be added to a scene.
		 *
		 */
		virtual Ogre::OverlayElement *element()
		{
			return mElement;
		}
	
	protected:
		
		// Underlying overlay element
		Ogre::OverlayElement *mElement;
		
		// Material for element
		Ogre::MaterialPtr mMaterial;
	
	};

}
}
}

#endif

