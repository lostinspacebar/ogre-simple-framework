/*
 * Base OGRE application. This creates an empty scene / window 
 * and calls the setup and loop functions. Real applications should 
 * override the default (empty) functionality of setup and loop.
 * 
 * ------------------------------------------------------------------
 * Parallax Ogre3D Helper Framework
 * Copyright 2013
 * Aditya Gaddam
 * http://lostinspacebar.com/project/parallax
 */

#include <Parallax/BaseApplication.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayContainer.h>
#include <OgreColourValue.h>
#include <OgreFontManager.h>

namespace lostinspacebar
{
namespace parallax
{
	BaseApplication::BaseApplication()
	{
		applicationName = "Basic OGRE Application";
	}

	BaseApplication::~BaseApplication()
	{
		delete mRoot;
	}

	bool BaseApplication::go()
	{
		// Initialize the root ogre object
		mRoot = new Ogre::Root("config/plugins.cfg");
	
		// Setup resources
		this->setupResources();
	
		// Setup render system
		this->setupRenderSystem();
	
		// Initialize the render window
		mRenderWindow = mRoot->initialise(true, this->applicationName);
	
		// Initialize resources (this doesn't actually load any resources)
		Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	
		// Create the default scene manager. Most applications will only need one.
		mSceneManager = mRoot->createSceneManager("DefaultSceneManager");
	
		// Create the default camera. Again, most applications should only need one.
		mCamera = mSceneManager->createCamera("DefaultCamera");
	
		// Create the default viewport.
		mViewport = mRenderWindow->addViewport(mCamera);
		
		// Get the overlay manager. We'll setup a default overlay for gui elements
		Ogre::OverlayManager *overlayManager = Ogre::OverlayManager::getSingletonPtr();
 
		// Create the default overlay and panel
		mDefaultOverlayPanel = static_cast<Ogre::OverlayContainer *>(overlayManager->createOverlayElement("Panel", "DefaultOverlayPanel"));
		mDefaultOverlayPanel->setMetricsMode(Ogre::GMM_RELATIVE);
		mDefaultOverlayPanel->setPosition(0, 0);
		mDefaultOverlayPanel->setDimensions(1.0, 1.0);
		Ogre::Overlay* overlay = overlayManager->create("DefaultOverlay");
		overlay->add2D(mDefaultOverlayPanel);
		
		// Show the overlay
		overlay->show();
	
		// Setup world
		this->setup();
	
		// Game loop
		while(true)
		{
			// Pump window messages for nice behaviour
			Ogre::WindowEventUtilities::messagePump();
		
			// Make sure window isn't closed
			if(mRenderWindow->isClosed())
			{
				return false;
			}
		
			// Game loop
			this->loop();
		
			// Render a frame
			if(!mRoot->renderOneFrame())
			{
				return false;
			}
		}
	
		// All done.
		return true;
	}

	bool BaseApplication::setupResources()
	{
		Ogre::ConfigFile configFile;
		configFile.load("config/resources.cfg");
	
		// Go through all sections & settings in the file
		Ogre::ConfigFile::SectionIterator seci = configFile.getSectionIterator();
		Ogre::String secName, typeName, archName;
		while (seci.hasMoreElements())
		{
			secName = seci.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;
			for (i = settings->begin(); i != settings->end(); ++i)
			{
				typeName = i->first;
				archName = i->second;
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
			}
		}
	
		return true;
	}

	bool BaseApplication::setupRenderSystem()
	{
		// Load config if it's there... otherwise show the config dialog
		if(!(mRoot->restoreConfig() || mRoot->showConfigDialog()))
		{
			return false;
		}
		
		// All done
		return true;
	}

	void BaseApplication::setup()
	{
		// Position it at 80 in Z direction
		mCamera->setPosition(Ogre::Vector3(0,0,80));
		// Look back along -Z
		mCamera->lookAt(Ogre::Vector3(0,0,-300));
		mCamera->setNearClipDistance(5);
	
		// Red bckground
		mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0));
	
		// Setup camera for viewport
		mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) / Ogre::Real(mViewport->getActualHeight()));
		 
		// Set ambient light
		mSceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
		 
		// Create a light
		Ogre::Light* l = mSceneManager->createLight("MainLight");
		l->setPosition(20,80,50);
	}

	void BaseApplication::loop()
	{
	
	}

}
}
