/*
 * Base OGRE application. This creates an empty scene / window 
 * and calls the setup and loop functions. Real applications should 
 * override the default (empty) functionality of setup and loop.
 * 
 * ------------------------------------------------------------------
 * Parallax Ogre3D Helper Framework
 * Copyright 2013
 * Aditya Gaddam
 * http://lostinspacebar.com/project/parallax
 */

#ifndef PARALLAX_BASEAPPLICATION_H_
#define PARALLAX_BASEAPPLICATION_H_

#include <OgreRoot.h>
#include <OgreConfigFile.h>
#include <OgreException.h>
#include <OgreCamera.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreEntity.h>
#include <OgreWindowEventUtilities.h>
#include <OgreOverlayContainer.h>
#include <OgreTextAreaOverlayElement.h>

namespace lostinspacebar
{
namespace parallax
{
	class BaseApplication
	{
	
	public:
	
		BaseApplication();
		~BaseApplication();
	
		/**
		 * Starts the initialization process + game loop
		 */
		virtual bool go();
	
	protected:
	
		/**
		 * Setup / Load resource information
		 */
		virtual bool setupResources();
	
		/**
		 * Setup the render system for OGRE
		 */
		virtual bool setupRenderSystem();
	
		/**
		 * Game world setup
		 */
		virtual void setup();
	
		/**
		 * Main game loop
		 */
		virtual void loop();
	
		// Application name
		Ogre::String applicationName;
	
		// Ogre component instances. Most of these are initialized in go()
		Ogre::Root 						*mRoot;
		Ogre::RenderWindow 				*mRenderWindow;
		Ogre::SceneManager 				*mSceneManager;
		Ogre::Camera 					*mCamera;
		Ogre::Viewport 					*mViewport;
		Ogre::OverlayContainer 			*mDefaultOverlayPanel;
	
	};

}
}

#endif /* BASEAPPLICATION_H_ */
