/*
 * Simple 2D Panel
 * 
 * ------------------------------------------------------------------
 * Parallax Ogre3D Helper Framework
 * Copyright 2013
 * Aditya Gaddam
 * http://lostinspacebar.com/project/parallax
 */

#include <Parallax/GUI/Panel.h>
#include <OgreOverlayManager.h>

namespace lostinspacebar
{
namespace parallax
{
namespace gui
{
	Panel::Panel(Ogre::String name, Ogre::Vector2 position, Ogre::Vector2 size, Ogre::ColourValue colour) : Element(name)
	{
		// Create material for the panel
		mMaterial = Ogre::MaterialManager::getSingleton().create(name + "_material", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, true);
		Ogre::Pass *pass = mMaterial->getTechnique( 0 )->getPass( 0 );
		pass->setLightingEnabled(false);
		pass->setDepthCheckEnabled(true);
		pass->setDepthWriteEnabled(false);
		
		// Create overlay element
		mElement = Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", name);
		mElement->setMaterialName(mMaterial->getName());
		
		// Everything is in pixels
		mElement->setMetricsMode(Ogre::GMM_PIXELS);
	
		// Set initial parameters
		setSize(size);
		setPosition(position);
		setBackgroundColour(colour);
	}
}
}
}
