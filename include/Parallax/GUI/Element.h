/*
 * Base on-screen entity
 * 
 * ------------------------------------------------------------------
 * Parallax Ogre3D Helper Framework
 * Copyright 2013
 * Aditya Gaddam
 * http://lostinspacebar.com/project/parallax
 */
 
#ifndef PARALLAX_GUI_ELEMENT_H_
#define PARALLAX_GUI_ELEMENT_H_

#include <OgreVector2.h>
#include <OgreColourValue.h>

namespace lostinspacebar
{
namespace parallax
{
namespace gui
{
	class Element
	{
	public:
		
		/**
		 * Constructor
		 *
		 * @param name		Element name
		 */
		Element(Ogre::String name)
		{
			
		}
		
		/**
		 * Gets the position of the element on screen
		 *
		 */
		virtual Ogre::Vector2 getPosition()
		{
			return mPosition;
		}
		
		/**
		 * Sets the position of the element on screen
		 *
		 */
		virtual void setPosition(Ogre::Vector2 position)
		{
			mPosition = position;
		}
		
		/**
		 * Gets the size of the element on screen
		 *
		 */
		virtual Ogre::Vector2 getSize()
		{
			return mSize;
		}
		
		/**
		 * Sets the size of the element on screen
		 *
		 */
		virtual void setSize(Ogre::Vector2 size)
		{
			mSize = size;
		}
		
		
		/**
		 * Gets the background colour of the element on screen
		 *
		 */
		virtual Ogre::ColourValue getBackgroundColour()
		{
			return mBackgroundColour;
		}
		
		/**
		 * Sets the background color for the element
		 *
		 * @param colour	Colour for the background of the element
		 */
		virtual void setBackgroundColour(Ogre::ColourValue colour)
		{
			mBackgroundColour = colour;
		}
	
	protected:
		
		Ogre::Vector2 		mPosition;
		Ogre::Vector2 		mSize;
		Ogre::ColourValue 	mBackgroundColour;
		
	
	};

}
}
}

#endif

