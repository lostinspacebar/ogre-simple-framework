/*
 * Simple test driver.
 * 
 */

#include "BaseApplication.h"

using namespace lostinspacebar::ogre;

int main(int argc, char *argv[])
{
	BaseApplication app;
	
	try
	{
		app.go();
	}
	catch(Ogre::Exception& e)
	{
		
	}
}

